package main;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Store Application Class
 * Used to call Cash Register, FoodInventory, and Product Classes to implement store
 */
public class StoreApplication {
	
	public static void main(String args[]) throws IOException {
		
		//create inventory
			InputStream input = inputStreamFromString("upc,name,wholesalePrice,retailPrice,quantity\n" + 
					"A123,Apple,0.50,1.00,100\n"+"O123,Orange,0.40,1.25,120\n"+"A246,Almonds,0.05,0.5,300\n"+
					"B23,Banana,0.15,1.00,200");
			
			FoodInventory inventory = new FoodInventory();
			inventory.replenish(input);
			
			System.out.println("Inventory System------------\n\nThe below products are in stock:\n");
			for (Product product : inventory.list()) {
				System.out.println(product);
			}
			
		//create customer items
			String customerItems[] = new String[] {"A123", "A123", "A123", "O123", "B23", "B23", "B23", "B23"};
			CashRegister register = new CashRegister(inventory);
			
		//begin transaction and scan customer items	
			register.beginTransaction();
			for (String upc : customerItems)
			{
				register.scan(upc);
			}
			
		//customer pays	w/$20.00
			BigDecimal customerMoney = new BigDecimal("20.00");
			register.pay(customerMoney);
			
		//print receipt and end transaction
			register.printReceipt(System.out);
			
		} 
	
	/**
	 * method to convert a string to an input stream.
	 * 
	 */
	public static InputStream inputStreamFromString(String value) throws UnsupportedEncodingException {
		return new ByteArrayInputStream(value.getBytes("UTF-8"));
	}
}