package main;

import java.io.InputStream;
import java.util.List;

/**
 * Interface that represents a product inventory.
 */
public interface Inventory {

	/**
	 * Reads the inventory from a comma separated InputStream, each
	 * line in the InputStream represents a different Product.
	 * example input stream looks like this:
	 * 
	upc,name,wholesalePrice,retailPrice,quantity
	A123,Apple,0.50,1.00,100
	B234,Peach,0.35,0.75,200
	C123,Milk,2.15,4.50,40
	 * 
	 * 
	 *inputStream the stream from where to read the inventory
	 */
	public void replenish(InputStream inputStream);

	/**
	 *returns an unmodifiable List of Product items representing 
	 * products inside the inventory.
	 */
	public List<Product> list();
}
