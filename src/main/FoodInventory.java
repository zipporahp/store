package main;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FoodInventory implements Inventory {

	private List <Product> ProductList = new ArrayList<Product>();
	
    @Override
    public void replenish(InputStream inputStream) {
   
    	String prod [] = new String[5];
		
    	
    	//get product details from Input stream using Scanner
		try {
			Scanner scanner = new Scanner(inputStream);
			scanner.nextLine();
			
			while(scanner.hasNextLine())
			{
				Scanner lineScanner = new Scanner(scanner.nextLine());
				lineScanner.useDelimiter(",");
				int i = 0;
				
				while(lineScanner.hasNext())
				{	
					for (; i< prod.length; i++ )
					{
						prod[i] = lineScanner.next();
					}
				}
				
				//create Product Object from array values and add to the product list for the inventory
				Product p = new Product(prod[0], prod[1], BigDecimal.valueOf(Double.parseDouble(prod[2])), BigDecimal.valueOf(Double.parseDouble(prod[3])), Integer.parseInt(prod[4]));
				ProductList.add(p);	
				lineScanner.close();			
			}
			
			scanner.close();
			
			}
			catch (Exception e) { System.out.println("Uh oh. Something went wrong: " + e) ; }
    }
    
	
    @Override
    public List<Product> list() {
        return ProductList;
    }
    

    
}
