package main;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Product {
	
	private String upc;
	private String name;
	private BigDecimal wholesalePrice;
	private BigDecimal retailPrice;
	private Integer quanity;
		
	//Product Constructor -- creates product 
	public Product (String upc, String name, BigDecimal price1, 
			BigDecimal price2, Integer quanity)
	{
		this.setUPC(upc);
		this.setName(name);
		this.setWholeSalePrice(price1);
		this.setRetailPrice(price2);
		this.setQuanity(quanity);
	}
	
	//no value constructor with default values
	//public Product () {}
	
	/*getters and setters */
	public void setUPC (String upc)
	{
		this.upc = upc;
	}
	
	public String getUPC ()
	{
		return upc; 
	}
	
	public void setName (String name)
	{
		this.name = name;
	}
	
	public String getName ()
	{
		return name;
	}
	
	public void setWholeSalePrice (BigDecimal price)
	{
		this.wholesalePrice = price.setScale(2, RoundingMode.HALF_UP);
	}
	
	public BigDecimal getWholeSalePrice ()
	{
		return wholesalePrice;
	}
	
	public void setRetailPrice (BigDecimal price)
	{
		this.retailPrice = price.setScale(2, RoundingMode.HALF_UP);
	}
	
	public BigDecimal getRetailPrice ()
	{
		return retailPrice;
	}
	
	public void setQuanity (Integer num)
	{
		this.quanity = num;
	}
	
	public Integer getQuanity ()
	{
		return quanity;
	}

	//new toString method to help with printing Products details better
	public String toString()
	{
		return "UPC: " + getUPC() + ", Product Name: " + getName() + ", Wholesale Price: $"
				+ getWholeSalePrice() + ", Retail Price: $" + getRetailPrice() + ", Quanity: " + getQuanity();
	}
}