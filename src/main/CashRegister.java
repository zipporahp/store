package main;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

/*
 *  CashRegister that accepts an Inventory as part of its constructor. 
 *  The CashRegister will be used to calculate the total price of Product bought by a user
 */
public class CashRegister {

	private FoodInventory inventory;
	private List <String> shoppingCart;
	private Dictionary<String, Integer> items; 
	private BigDecimal amountPaid;
		
	//constructor that takes an inventory parameter
	public CashRegister (FoodInventory inventory)
	{
		this.inventory = inventory;	
	}
	
	public List<Product> getInventory(){
		return inventory.list();
	}
	
	//method to create a new transaction and setup the Cash Register
	public void beginTransaction()
	{
		shoppingCart = new ArrayList <String>();
		items = new Hashtable<String, Integer>();
	}
	
	public boolean scan(String upc)
	{
		
		for (Product p : inventory.list())
		{
			//product scanned is in inventory
			if (upc.equals(p.getUPC()))
			{
				shoppingCart.add(p.getUPC());
				return true;
			}
		}
		
		//product scanned is not in inventory
		return false;
	}
	
	public BigDecimal getTotal() throws NullPointerException 
	{
		BigDecimal totalCost = new BigDecimal("0");
		
		//calculate how many of each products are in shopping cart
		//and store in dictionary
		for(int i = 0; i < shoppingCart.size(); i++)
		{
			int freq = Collections.frequency(shoppingCart, shoppingCart.get(i));
			items.put(shoppingCart.get(i), freq);
		}
		
		/*calculate total cost from shopping cart products 
			and inventory stored product's retail price
			
			triple for loop:
			outer: checks inventory system for a match to the customer's shopping cart products upcs for product retail price
			middle: loops through customer shopping cart
			inner:  helps prevent duplicate total cost calculations when comparing UPCs in the shoppping cart      
		*/
		int i,k;
		for (Product p : inventory.list()) {
			for(i=0; i < shoppingCart.size(); i++){	
				for( k=0 ; k<i ; k++)
				{
					if(shoppingCart.get(i).equals(shoppingCart.get(k)))
					{
						break; //don't calculate UPCs that have already been calculated 
					}
				}
				if(i == k){
					if (shoppingCart.get(i).equals(p.getUPC()))
					{
						BigDecimal retailPrice = p.getRetailPrice();
						BigDecimal productCount = BigDecimal.valueOf(items.get(p.getUPC()));
						totalCost = totalCost.add(retailPrice.multiply(productCount));
					}
				} 
			}
		}
		return totalCost;
	}
	
	public BigDecimal pay(BigDecimal cashAmount) throws NumberFormatException
	{
		amountPaid = cashAmount;
		return cashAmount.subtract(getTotal());
	}
	
	public void printReceipt(OutputStream os)
	{
		PrintWriter writer = new PrintWriter(os, true);
		
		writer.append("\nWelcome to the Convenience Store\n");
		writer.append("-----------------------------\n");
		
		//print the products bought and their quantity in the shopping cart with price
		writer.append("Total Products Bought: "+shoppingCart.size()+"\n");
		writer.append("\n");
		
		int i,k;
		for (Product p : inventory.list()) {
			for(i = 0; i < shoppingCart.size(); i++) {
				for(k=0 ; k<i ; k++)
				{
					if (shoppingCart.get(i).equals(shoppingCart.get(k)))
					{
						break; //don't print anything if UPCs are the same
					}
				}
				if(i==k)
				{	
					//print distinct Products in the shopping cart w/their purchase quanity & retail price
					if (shoppingCart.get(i).equals(p.getUPC()))
					{
						BigDecimal retailPrice = p.getRetailPrice();
						BigDecimal productCount = BigDecimal.valueOf(items.get(p.getUPC()));
						writer.printf("%d %s @ $%.2f: $%.2f%n", items.get(p.getUPC()) ,p.getName(), 
									p.getRetailPrice(), (retailPrice.multiply(productCount)));
					}
				}
			}
		}
		//total amount of everything bought, paid and change owed
		writer.append("-----------------------------\n");
		writer.printf("Total: $%.2f%n", getTotal());
		writer.printf("Paid: $%.2f%n", amountPaid);
		writer.printf("Change: $%.2f%n", pay(amountPaid));
		writer.append("-----------------------------\n");	
		writer.append("\nThank you for shopping! Hope to see you again soon!");
		writer.close();
	}
}