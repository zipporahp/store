package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

import java.io.*;
import java.util.*;
import java.math.BigDecimal;
import main.*;

class CashRegisterTest{
	
	private CashRegister testRegister;
	private InputStream testInput;
	private FoodInventory testInventory = new FoodInventory();
	private List <String> testShpgCrt;
	private Dictionary<String, Integer> items; 
	private BigDecimal amountPaid; 
	private BigDecimal totalAmt;
	
	@Test
	@Order(1)
	void testConstructor()throws UnsupportedEncodingException {
		
		testInput = StoreApplication.inputStreamFromString("upc,name,wholesalePrice,retailPrice,quantity\n" + 
				"A123,Apple,0.50,1.00,100\n"+"O123,Orange,0.50,1.00,120\n"+"A246,Almonds,0.50,1.00,300\n"+
				"B23,Banana,0.50,1.00,200");
		testInventory.replenish(testInput);
		testRegister = new CashRegister(testInventory);
		assertEquals(testRegister.getInventory(), testInventory.list());
	}
	
	@Test
	@Order(2)
	void testBeginTransaction(){
		testRegister.beginTransaction();
		assertNotNull(testShpgCrt);
		assertNotNull(items);		
	}
	
	@Test
	@Order(3)
	void testScan() {
		BigDecimal wholesale1 = new BigDecimal("1.50");
		BigDecimal retail1 = new BigDecimal("4.75");
		Integer quanity = Integer.valueOf(1);
		Product orange = new Product("O123", "Orange", wholesale1, retail1, quanity);
		Product apple = new Product("A123", "Apple", wholesale1, retail1, quanity);
		Product avocado = new Product("AV123", "Avocado", wholesale1, retail1, quanity);
		assertTrue(testRegister.scan(orange.getUPC()));
		assertTrue(testRegister.scan(apple.getUPC()));
		assertFalse(testRegister.scan(avocado.getUPC()));
		
	}
	
	@Test
	@Order(4)
	void testGetTotal() {
		totalAmt = testRegister.getTotal();
		assertNotNull(testShpgCrt);		
		assertTrue(testShpgCrt.size()>0);	
	}
	
	@Test
	@Order(5)
	void testPay() {
		amountPaid = new BigDecimal("20.00");
		BigDecimal changeAmt = testRegister.pay(amountPaid);
		assertTrue(totalAmt<=amountPaid);
		assertEqual(changeAmt,(amountPaid-totalAmt));				
	}
	
	@Test
	@Order(6)
	void testPrintReceipt(){
		assertNotNull(testRegister.printReceipt(System.out));
	}

}
