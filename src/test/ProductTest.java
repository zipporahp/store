package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Order;
import java.math.BigDecimal;
import main.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProductTest {
	
	private BigDecimal wholesale;
	private BigDecimal retail;
	private Product testProduct;
	private Integer quanity;
	
	@Test
	@Order(1)
	void testConstructor(){
		wholesale = new BigDecimal("1.50");
		retail = new BigDecimal("4.75");
		quanity = Integer.valueOf(3);
		testProduct = new Product("123456789", "Orange", wholesale, retail, quanity);
		assertEquals("123456789", testProduct.getUPC());
		assertEquals("Orange", testProduct.getName());
		assertEquals(wholesale, testProduct.getWholeSalePrice());
		assertEquals(retail, testProduct.getRetailPrice());
		assertEquals(quanity, testProduct.getQuanity());
	}
	
	@Test
	@Order(2)
	void testToString() {
		String testString = "UPC: 123456789, Product Name: Orange, Wholesale Price: $1.50, Retail Price: $4.75, Quanity: 3";
		assertEquals(testString, testProduct.toString());
	}

}
