package test;

import static org.junit.jupiter.api.Assertions.*;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import main.*;

public class FoodInventoryTest {

	private FoodInventory testInventory = new FoodInventory();
	private InputStream testInput; 
	private List <Product> testProductList = new ArrayList<Product>();
	
	@Test
	@DisplayName("Replenish() test case")
	void testReplenish() throws UnsupportedEncodingException {
		testInput = StoreApplication.inputStreamFromString("upc,name,wholesalePrice,retailPrice,quantity\n" + 
				"A123,Apple,0.50,1.00,100\n"+"O123,Orange,0.50,1.00,120\n"+"A246,Almonds,0.50,1.00,300\n"+
				"B23,Banana,0.50,1.00,200");
		testInventory.replenish(testInput);
		
		BigDecimal wholesale = new BigDecimal("0.50");
		BigDecimal retail = new BigDecimal("1.00");
		Product apple = new Product("A123", "Apple", wholesale, retail, 100);
		Product orange = new Product("O123", "Orange", wholesale, retail, 120);
		Product almonds = new Product("A246", "Almonds", wholesale, retail, 300);
		Product banana = new Product("B23", "Banana", wholesale, retail, 200);
		
		testProductList.add(apple);
		testProductList.add(orange);
		testProductList.add(almonds);
		testProductList.add(banana);
		
		assertTrue(testProductList.size() == testInventory.list().size());
		assertEquals(apple, testInventory.list().get(0));
		assertEquals(orange, testInventory.list().get(1));
		assertEquals(almonds, testInventory.list().get(2));
		assertEquals(banana, testInventory.list().get(3));

	}
	
	@Test
	@DisplayName("List() test case")
	void testList() {
		assertNotNull(testProductList);
	}

}
